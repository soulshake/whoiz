#!/usr/bin/env python

from __future__ import print_function
import sys
import click
import xmlrpclib
import random
import os
import commands

script_path = os.path.dirname(os.path.realpath(__file__))

def add_garbage(garbage=set()):
    # Accept as input lines which should be filtered from future whois lookups
    enter_pressed = 0
    print("Paste lines from whois output to filter from future whois lookups (enter twice to end):")
    while enter_pressed < 2:
        line = raw_input()
        if line == '':
            enter_pressed += 1
        else:
            enter_pressed = 0
            garbage.add(line)
    print("adding {} lines to garbage file...\n".format(len(garbage)))
#<<<<<<< HEAD
    garbage_file = script_path + "garbage.txt"
    with open(garbage_file, "a") as f:
#=======
#    with open(GARBAGE_FILE, "a") as f:
#>>>>>>> fe45621a757ef0b0d07845c81b349e938540b831
        for line in garbage:
            f.write(line + "\n")


@click.command()
@click.option('-v', '--verbose', is_flag=True,
              help="Display (dim) garbage lines instead of hiding them")
@click.option('-a', '--add', is_flag=True,
              help="Provide lines to add to the garbage file.")
@click.option('-g', '--gandi', is_flag=True,
              help="Query whois.gandi.net")
@click.option('-h', '--host',
              help="Query a specific whois server")
@click.argument('domain', required=True)
def whois(domain, verbose, add, gandi, host):
    """ Do a whois lookup on the domain and return the output, or just add
    lines to garbage file"""

    whois_opts = " -h {}".format(host) if host else ""

    # touch garbage.txt to create it if it doesn't exist
    cmd = "touch" + script_path + "garbage.txt"
    commands.getoutput(cmd)

    # See if user just wants to add more lines to the garbage file
    if add:
        print("adding garbage.")
        add_garbage()
    else:
        print("Doing a whois lookup on {}...".format(domain))
        status, whois_output = commands.getstatusoutput("whois {} {}"
                .format(domain, whois_opts))

        if status == 0 and whois_output != 'NOT FOUND':
            filter(whois_output, verbose)
        elif status == 256:
            result = api.domain.available(apikey, [domain])
            if result[domain] == "error_invalid":
                print("{} returned status {}, domain availability check returned {}".format(domain, status, result[domain]))
                filter(whois_output, verbose)
            else:
                if result[domain] == 'pending':
                    print(result)
                else:
                    print("{} is {}.".format(domain, result[domain]))
                register(domain, result)
        elif status == 512:
            # print(red + "Your IP has been restricted due to excessive access, or this: fgets: Connection reset by peer" + black)
            filter(whois_output, verbose)
        else:
            print("Something weird happened with the whois, got status {}.".format(status))
            register(domain)

def filter(whois_output, verbose):
    with open(script_path + "garbage.txt", "r") as f:
        garbage = set(f.read().split("\n"))

    garbage.remove('')

    gray = "\033[90m"
    black = "\033[0m"
    red = "\033[91m"
    yellow = "\033[93m"
    green = "\033[92m"

    whois_list = []
    with open(script_path + '/' + "whois.txt", "a") as w:
        for line in whois_output.split("\n"):
            whois_list.append(line.strip()) # print(line)
        for line in whois_list:
            if line in garbage:
                if verbose:
                    print(yellow + line + black)
                    w.write(line + "\n")
            elif "registrar:" in line.lower():
                w.write(line + "\n")
                print(red + line + black)
            elif "fgets:" in line.lower():
                w.write(line + "\n")
                print(red + line + black)
            else:
                w.write("     " + line + "\n")
                print(green + line + black)


def register(domain, result=None):
    # Print domain info: availability status, price for registration
    # Print instructions to register domain:

    product_spec = {
      'product' : {
        'description': domain,
        'type':'domains'
      },
      'action': {
        'name':'create',
        'duration': 1
      }
    }

    catalog = api.catalog.list(apikey, product_spec)
    price = catalog[0]['unit_price'][0]['price']
    currency = catalog[0]['unit_price'][0]['currency']
    grid = catalog[0]['unit_price'][0]['grid']
    duration = catalog[0]['unit_price'][0]['min_duration']
    print("{} can be registered for {} year for {} {} at {} rates"
            .format(domain, duration, price, currency, grid))

    url = "https://www.gandi.net/domain/suggest?domain_list=" + domain
    print("You can try to register it here: \n{}".format(url))

api = xmlrpclib.ServerProxy('https://rpc.gandi.net/xmlrpc/')
apikey = os.getenv('GANDI_API_KEY')

if __name__ == '__main__':

    whois()

    # TODO: check currency, rate scale


